import React from 'react';

import {
    View,
    Image
} from 'react-native';

import {
  Content,
  Header,
  Left,
  Button,
  Body,
  Title,
  Right,
} from "native-base";

import { Constants } from 'expo';

// Icons
import { MaterialIcons, createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/project_icons.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'Icons');

export class DrawerHeader extends React.Component {

  constructor() {
    super();
    this.state = {};
  };

  render() {

    return (
      <View style={{marginTop: Constants.statusBarHeight}}>
        <Header style={{backgroundColor: 'white'}}>
          <Left style={{flex: 0}}>
            <Button transparent
              onPress={() => {
                this.props.navigation.navigate('DrawerToggle');
              }}
            >
              <MaterialIcons name='menu' size={30} color='#b5b5b5' />
            </Button>
          </Left>
          <Body>
            <Image
              source={require('../assets/images/sem_parar_logo.png')}
              fadeDuration={0}
              style={{width: 37, height: 30, alignSelf: 'center'}}
            />
          </Body>
          <Right style={{flex: 0}}>
            <Button transparent>
              <MaterialIcons name='notifications' size={20} color='#b5b5b5' />
            </Button>
          </Right>
        </Header>
      </View>
    )
  }
}