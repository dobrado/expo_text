import color from "color";

import {
  Platform,
  Dimensions,
  PixelRatio
} from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = "material";
const isIphoneX =
  platform === "ios" && deviceHeight === 812 && deviceWidth === 375;

export default {
  "CheckboxRadius": 0,
  "CheckboxBorderWidth": 1,
  "CheckboxPaddingLeft": 0,
  "CheckboxPaddingBottom": 0,
  "CheckboxIconSize": 10,
  "CheckboxFontSize": 16,
  "checkboxBgColor": "rgba(255,255,255,1)",
  "checkboxSize": 13,
  "checkboxTickColor": "rgba(0,0,0,1)"
}