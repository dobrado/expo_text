import React from 'react';
import { DrawerNavigator } from 'react-navigation';

import Screen1 from '../screens/EmptyScreen';
import Screen2 from '../screens/EmptyScreen';
import Screen3 from '../screens/EmptyScreen';

const DrawerScreen = DrawerNavigator({
  Screen1: {
    screen: Screen1,
    navigationOptions: {
      header: null
    },
  },
  Screen2: {
    screen: Screen2,
    navigationOptions: {
      header: null
    },
  },
  Screen3: {
    screen: Screen3,
    navigationOptions: {
      header: null
    },
  },
})
DrawerScreen.navigationOptions = {
  header: null
};

export default DrawerScreen;
