import React from 'react';

import {
    View,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import {
  Content,
  Text // Always use Text component from Native-base
} from "native-base";

// This is the icons from project.
// If the layout has icons, please use similar icons from: https://expo.github.io/vector-icons/
// Just add icon name in the line below
import { MaterialCommunityIcons, createIconSetFromIcoMoon } from '@expo/vector-icons';
// example: import { MaterialCommunityIcons, createIconSetFromIcoMoon }
import icoMoonConfig from '../assets/fonts/project_icons.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'Icons');


// Use the same name of component as file name.
export default class EmptyName extends React.Component {
  static navigationOptions = {
    title: 'Title',
    headerStyle: {
        backgroundColor: '#3232a8'
    },
    headerTintColor: '#fff'
  };

  constructor() {
    super();
    this.state = {};
  };

  // Create the functions like: 
  _nullFunction () {

  }

  render() {

    return (
      <View style={styles.container}>
        <Content contentContainerStyle={{ flex: 1 }}>
          <TouchableOpacity onPress={() => this._nullFunction()}>
            <Text>Empty Screen</Text>
          </TouchableOpacity>
        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff'
    }
  });